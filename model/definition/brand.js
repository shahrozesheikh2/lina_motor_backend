'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../../common/databaseConnection")


const Brand = sequelize.define('Brand', {
    // Model attributes are defined her
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    }
}, {
    // Other model options go here
    timestamps: true,
    paranoid: true,
    tableName: 'brand'
});

module.exports = Brand;