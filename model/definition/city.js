'use strict'
const  DataTypes  = require('sequelize');
const sequelize = require("../../common/databaseConnection")

const City = sequelize.define('City', {
    // Model attributes are defined here
    cityName: {
        type: DataTypes.STRING,
        unique: true
    }
}, {
    timestamps: true,
    paranoid: true,
    tableName: 'city'
});
console.log("City Table", City === sequelize.models.City);

module.exports = City;