
const  DataTypes  = require("sequelize");
const sequelize = require("../../common/databaseConnection")

const User = sequelize.define('User', {
    id: {
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        type: DataTypes.INTEGER
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: false,
        unique: {
        msg: 'Email Already registered'
        }
    },
    fullName: {
        type: DataTypes.STRING,
        allowNull: true
    },
    phoneNumber: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    isActive: {
        type: DataTypes.TINYINT,
        allowNull: true,
        defaultValue: 0
    },
}, {
    tableName: 'user',
    timestamps: true,
    paranoid: true,
})
console.log("User Table", User === sequelize.models.User);
module.exports = User;