const jwt = require("jsonwebtoken");
const userService = require('../service/userService')
const config = require('../config.json');



module.exports = {
    userLogin: async function(email){
        // var userLogin = await userService.userLogin(email);
            var user = await userService.addUser(email)
            let token = jwt.sign({
                id: user.id,
                email:user.email
            }, config.jwt.secret, { expiresIn: '12D' })
            return{token}
    }
}