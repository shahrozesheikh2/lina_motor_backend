var jwt = require("jsonwebtoken");
const config = require('../config.json');

exports.verifyJwt = async (req, res, next) => {
    if (!req.headers['authorization']) {
        return res.status(401).send();
    }
    try {
        let authorization = req.headers['authorization'].split(' ');
        var decoded = await jwt.verify(authorization[1], config.jwt.secret);
        
        req.headers.userId = decoded.id;

        return next();

    } catch (err) {
        res.status(StatusCodes.METHOD_NOT_ALLOWED).send({
            data: {},
            message: err.message,
            error: err.stack
        })
    }

}