const { Op } = require("sequelize");
const model = require("../model");

try{
    module.exports= {
        userLogin: async function (email){
            return user = await model.user.findOne({
                where:{
                    [Op.or]:[{
                        email
                    }]
                }
            })
        },
        addUser: async function (email) {

            return await model.user.findCreateFind({
                where: {
                    email: email
                },
                email,
                createdAt: new Date(),
                })
        },
    }
}catch(error){
    throw new Error(error)
}