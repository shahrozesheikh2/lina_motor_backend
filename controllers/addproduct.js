const userModel = require('../model/userModel');
const Joi = require('joi');

const schema = Joi.object().keys({
    name: Joi.string().required(),
    price:Joi.number().integer().min(1).required(),
    year:Joi.number().min(1900).max(3000).required(),
    phoneNumber:Joi.number().required(),
    // status:Joi.number().integer().required(),
    description:Joi.string().required(),
    resources: Joi.array().items(Joi.object({
        awsKey: Joi.string().required(),
        awsUrl: Joi.string().uri().required()
    }).required()).max(7).required(),
    brandId: Joi.number().required(),
    cityId:Joi.number().required()
    


})

const {
    ReasonPhrases,
    StatusCodes,
    getReasonPhrase,
    getStatusCode,
} = require('http-status-codes');
const { JSONCookie } = require('cookie-parser');

module.exports = async function userLogin(req, res) {

    try {
        const validate = await schema.validateAsync(req.body, { abortEarly: false });
      
        if (validate.error) {
            res.status(StatusCodes.BAD_REQUEST).send({
                data: {},
                message: err.message,
                error: err.stack
            })
        }
        // const data = await userModel.userLogin(req.body.email);
        res.status(StatusCodes.OK).send({ message: "Bike Add Sucessfully", data, error: {} })

    }
    catch (err) {

        res.status(StatusCodes.METHOD_NOT_ALLOWED).send({
            data: {},
            message: err.message,
            error: err.stack
        })
    }
}