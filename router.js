const router = require('express').Router();

const { Router } = require('express');
const { route } = require('./app');
const { verifyJwt } = require("./common/authentication");

const { 
     userLogin,
     addProduct
} = require('./controllers/index');

router.post('/api/userLogin',userLogin);
router.post('/api/addproduct',verifyJwt,addProduct)


module.exports = router;